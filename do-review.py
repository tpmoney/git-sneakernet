#! /usr/bin/env python

import argparse
import os
import re
import subprocess

DEFAULT_BASE = "master"

def main():
	parser = argparse.ArgumentParser()
	parser.description = "Applies a patch bundle created with the make-review-patch script to the current git repository and runs the diff ranges specified"
	parser.add_argument("patchFile", help="The patch file to apply")
	parser.add_argument("-r", "--review-only", help="Only do the reviews requested in the patch file, don't apply the patches. The patch file must have already been applied.", action="store_true")

	args, unknownargs = parser.parse_known_args()

	base = None
	head = None
	ranges = None
	unique = "code-review-" + os.path.basename(args.patchFile)

	base_matcher = re.compile(r'^#BASE=(.+)')
	head_matcher = re.compile(r'^#HEAD=(.+)')
	range_matcher = re.compile(r'^#REVIEWS=(.+)')

	#Parse the patch file, looking for our base commit information and the review ranges
	in_header = False
	temp_file = args.patchFile + ".tmp"
	with open(args.patchFile, "r") as file, open(temp_file, "w") as temp:
		for line in file.readlines():
			if not in_header and "---SNEAKER NET HEADER---" in line:
				in_header = True

			elif in_header and "---SNEAKER NET HEADER---" not in line:
				if base == None:
					base_match = base_matcher.search(line)
					if base_match :
						base = base_match.group(1)
						continue

				if ranges == None:
					range_match = range_matcher.search(line)
					if range_match:
						ranges = range_match.group(1).split(";;")
						continue

				if head == None:
					head_match = head_matcher.search(line)
					if head_match:
						head = head_match.group(1)
						continue

			elif in_header and "---SNEAKER NET HEADER---" in line:
				in_header = False

				if base == None or ranges == None or head == None:
					print("Missing base, head or review tags in custom patch file SNEAKER NET HEADER. Can not complete.")
					quit()

			else:
				temp.write(line)

	#Create a branch for the patch
	if not args.review_only:
		subprocess.check_call(["git", "checkout", base])
		subprocess.check_call(["git", "fetch", temp_file, head + ":refs/heads/" + unique])

	subprocess.check_call(["git", "checkout", unique])
	os.remove(temp_file)

	print("\n\n")

	#Review the requested ranges
	for review in ranges :
		start_end = review.split(",")
		short_start = subprocess.check_output(["git", "rev-parse", "--short", start_end[0]]).strip()
		short_end = subprocess.check_output(["git", "rev-parse", "--short", start_end[1]]).strip()
		current_range = start_end[0] + ".." + start_end[1]
		current_range_short = short_start + ".." + short_end

		print("Beginning review for range: " + current_range_short)
		subprocess.check_call("git", "log", "--oneline", current_range)
		choice = raw_input("Begin (y/n/q): ")

		if choice.lower() == "y":
			subprocess.check_call(["git", "difftool", current_range])
		elif choice.lower() == "n":
			print("Skipping review for range " + current_range_short)
		elif choice.lower() == "q":
			print("Quitting, will not continue with reviews")
		else:
			print("Bade input: " + choice + ", quitting.")
			quit()

	print("Done with code review, to review again, run with the --review-only option")

if __name__ == '__main__':
	main()