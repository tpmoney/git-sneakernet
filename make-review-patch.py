#! /usr/bin/env python

import argparse
import os
import subprocess

DEFAULT_BASE = "master"

def main():
	parser = argparse.ArgumentParser()
	parser.description = "Generates a review patch bundle for git. The file is comparitlbe with the do-review script which enables the user to review specific diff ranges defined by the patch creator."
	parser.add_argument("toRef", help="The ref to run the patch to, usually this is the name of the branch you want to have reviewed")
	parser.add_argument("outFile", help="The name of the patch file to be created")
	parser.add_argument("-b", "--base", help="The base ref from which to generate the patch", default=DEFAULT_BASE)
	parser.add_argument("-r", "--range", help="A given range to review, can be specified multiple times. Takes the form of \"start-ref,end-ref\"", action="append")

	args, unknownargs = parser.parse_known_args()

	base_hash = subprocess.check_output(["git", "rev-parse", args.base]).strip()
	to_hash = subprocess.check_output(["git", "rev-parse", args.toRef]).strip()

	#If the user gave us ranges, use those, otherwise use base to ref as the review range
	# Use rev-parse to convert all refs to hash ids
	my_ranges = []
	if (not args.range) or (len(args.range) < 1):
		my_ranges = [base_hash + "," + to_hash]
	else:
		for curr_range in args.range:
			start_end = curr_range.split(",")
			start_com = subprocess.check_output(["git", "rev-parse", start_end[0]]).strip()
			end_com = subprocess.check_output(["git", "rev-parse", start_end[1]]).strip()
			my_ranges.append(start_com + "," + end_com)

	commit_range = args.base + ".."  + args.toRef

	base = "#BASE=" + base_hash
	head = "#HEAD=" + to_hash
	review_ranges = "#REVIEWS=" + ";;".join(my_ranges)

	our_header = "---SNEAKER NET HEADER---\n" + base + "\n" + head + "\n" + review_ranges + "\n---SNEAKER NET HEADER---\n"

	temp_file = args.outFile + ".tmp"

	subprocess.check_call(["git", "bundle", "create", temp_file, commit_range])
	with open(args.outFile, "w") as file, open(temp_file) as temp:
		file.write(our_header)
		for line in temp.readlines():
			file.write(line)

	os.remove(temp_file)

if __name__ == '__main__':
	main()